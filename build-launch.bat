docker stop lpmon
docker container rm lpmon
docker build -t lpmon .
docker run --rm -p 7890:7890 --name=lpmon -d lpmon
docker cp D:/coding/projects/davidsfreunde/viacash/lpmonitoring-containerized/id_rsa.pem lpmon:/root/.ssh/id_rsa.pem
docker exec lpmon chmod 600 /root/.ssh/id_rsa.pem
@REM docker exec lpmon /fetch-logs.sh
docker exec -it lpmon /bin/bash
@REM docker exec -it lpmon goaccess --log-format=COMMON -a /apachelogs/access_log
docker exec -it lpmon ssh -n -o -q StrictHostKeyChecking=no ubuntu@52.30.117.138 -i /root/.ssh/id_rsa.pem 'tail -f /opt/bitnami/apache2/logs/access_log' | goaccess --log-format=COMBINED -a