FROM ubuntu:latest
# installs
RUN wget -O - https://deb.goaccess.io/gnugpg.key | gpg --dearmor | tee /usr/share/keyrings/goaccess.gpg >/dev/null \
RUN echo "deb [signed-by=/usr/share/keyrings/goaccess.gpg] https://deb.goaccess.io/ $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/goaccess.list
RUN apt-get update
RUN apt-get -y install goaccess
RUN apt-get -y install ssh
RUN apt-get -y install cron
RUN apt-get -y install build-essential
RUN apt-get -y install libncursesw5-dev
RUN apt-get -y install libmaxminddb-dev

RUN wget https://tar.goaccess.io/goaccess-1.5.1.tar.gz && \
    tar -xzvf goaccess-1.5.1.tar.gz && \
    cd goaccess-1.5.1/ && \
    ./configure --enable-utf8 --enable-geoip=mmdb && \
    make && \
    make install

# cron job
RUN mkdir -p /root/.ssh
RUN mkdir -p /apachelogs # directory for logs
COPY fetch-logs.sh fetch-logs.sh
COPY apache-log-cron /etc/cron.d/apache-log-cron
RUN chmod 0644 /etc/cron.d/apache-log-cron
RUN crontab /etc/cron.d/apache-log-cron
CMD ["cron", "-f"]





